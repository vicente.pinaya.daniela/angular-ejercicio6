import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';


import { APP_ROUTING } from './app.routes';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
 
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }





























//problema 7
/* mensaje informativo  apret boton 1 = boton 1
mensaje informativo  apret boton 2 = boton 2
mensaje informativo  apret boton 3 = boton 3 .....no existe este caso por que no apretas el boton y se va por default

caso 6 se va por defecto  */
