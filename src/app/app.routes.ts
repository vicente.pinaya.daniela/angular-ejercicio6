import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./component/inicio/inicio.component";
import { ProductosComponent } from "./component/productos/productos.component";
import { ContactoComponent } from "./component/contacto/contacto.component";



import { Opcion1Component } from "./component/opcion1/opcion1.component";
import { Opcion2Component } from "./component/opcion2/opcion2.component";
import { Opcion3Component } from "./component/opcion3/opcion3.component";
import { Opcion4Component } from "./component/opcion4/opcion4.component";
import { Opcion5Component } from "./component/opcion5/opcion5.component";



const APP_ROUTES: Routes = [
  {path : 'inicio', component: InicioComponent},
  {path: 'productos', component: ProductosComponent},
  {path: 'contacto',component:ContactoComponent},

  {path:'opcion1', component:Opcion1Component},
  {path:'opcion2', component:Opcion2Component},
  {path:'opcion3', component:Opcion3Component},
  {path:'opcion4', component:Opcion4Component},
  {path:'opcion5', component:Opcion5Component},

  {path:'**',pathMatch:'full',redirectTo: 'inicio'}
];


// @NgModule((
//   imports : [RouterModule.forRoot(APP_ROUTES)],
//   exports : [RouterModule]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

// 